<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page = "../topo.xhtml" />

	<div class="container">
		<div class="main-page">
			<div class="row">
            	<h4 class="pull-left page-title">Cadastrar Processo &nbsp;&nbsp;</h3>
	                <ol class="breadcrumb pull-right">
	                    <li><a href="#">Collegialis</a></li>
	                    <li><a href="listar.jsp">Processos</a></li>
	                    <li class="active">Cadastrar</li>
	                </ol>
            </div>

			<!-- Mensagens de erro do formulario -->
			<c:if test="${not empty msgsErro}">
				<div style="color: red">
					<ul>
						<c:forEach var="msg" items="${msgsErro}">
							<li>${msg}</li>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Dados do processo</h3>
				</div>
				<div class="panel-body">

					<form action="${pageContext.request.contextPath}/controller.do" method="POST" class="form-horizontal">
						<input type="hidden" name="op" value="novproc">
						<div class="row">
							<div class="form-group">
								<label for="numero" class="col-md-2 control-label">N�mero do processo:</label>
								<div class="col-md-10">
									<input id="numero" value="${processo.numero}" name="numero" type="text" class="form-control" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="dataRecepcao" class="col-md-2 control-label">Data de Recebimento:</label>
								<fmt:formatDate var="dif" value="${processo.dataRecepcao}" pattern="dd/MM/yyyy"/>  
								<div class="col-md-10">
									<input id="dataRecepcao" value="${dif}" name="dataRecepcao" class="form-control" type="text" placeholder="dd/mm/aaaa" />
								</div>
							</div>
						</div>

						<div class="row">
							<div  class="form-group">
									<label class="col-md-2 control-label" for="requisitante">Aluno Requisitante:</label> 
									<div class="col-md-10">
										<select class="form-control" id="requisitante" name="requisitante">
										<c:forEach var="requisitante" items="${utilBean.alunos}">
											<c:if test="${requisitante.id eq processo.requisitante.id}">
												<option value="${requisitante.id}" label="${requisitante.nome}" selected>${requisitante.nome}</option>
											</c:if>
											<c:if test="${requisitante.id ne processo.requisitante.id}">
												<option value="${requisitante.id}" label="${requisitante.nome}">${requisitante.nome}</option>
											</c:if>
											
										</c:forEach>
										</select>
									</div>
							</div>
						</div>
						<div class="row">
							<div  class="form-group">
									<label class="col-md-2 control-label" for="assunto">Assunto:</label> 
									<div class="col-md-10">
										<select class="form-control" id="assunto" name="assunto">
										<c:forEach var="assunto" items="${utilBean.assuntos}">
											<c:if test="${assunto.id eq processo.assunto.id}">
												<option value="${assunto.id}" label="${assunto.descricao}" selected>${assunto.descricao}</option>
											</c:if>
											<c:if test="${assunto.id ne processo.assunto.id}">
												<option value="${assunto.id}" label="${assunto.descricao}">${assunto.descricao}</option>
											</c:if>
											
										</c:forEach>
										</select>
									</div>
							</div>
						</div>
						<div class="row">
							<div  class="form-group">
									<label class="col-md-2 control-label" for="relator">Relator:</label> 
									<div class="col-md-10">
										<select class="form-control" id="relator" name="relator">
										<c:forEach var="relator" items="${utilBean.relatores}">
											<c:if test="${relator.id eq processo.relator.id}">
												<option value="${relator.id}" label="${relator.nome}" selected>${relator.nome}</option>
											</c:if>
											<c:if test="${relator.id ne processo.relator.id}">
												<option value="${relator.id}" label="${relator.nome}">${relator.nome}</option>
											</c:if>
											
										</c:forEach>
										</select>
									</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<br /> <input type="submit" class="btn btn-success" value="Cadastrar">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
<jsp:include page = "../rodape.xhtml" />
	<c:set var="endofconversation" value="true" scope="request"/>

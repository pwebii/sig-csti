<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page = "../topo.xhtml" />

	<div class="container">
		<div class="main-page">
			<div class="row">
	                <h4 class="pull-left page-title">Colegiados &nbsp;&nbsp;
					<a href="cadastrar.jsp" class="btn btn-success btn-xs">+</a></h4>
	                <ol class="breadcrumb pull-right">
	                    <li><a href="#">Collegialis</a></li>
	                    <li><a href="#">Colegiados</a></li>
	                    <li class="active">Listar</li>
	                </ol>
            </div>

			<!-- Mensagens de erro do formulario -->
			<c:if test="${not empty msgsErro}">
				<div style="color: red">
					<ul>
						<c:forEach var="msg" items="${msgsErro}">
							<li>${msg}</li>
						</c:forEach>
					</ul>
				</div>
			</c:if>
			
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Descri��o</th>
								<th>Portaria</th>
								<th>Data in�cio</th>
								<th>Data fim</th>
								<th>Curso</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach var="colegiado" items="${utilBean.colegiados}">
							<tr>
							<td>${colegiado.descricao}</td>
							<td>${colegiado.portaria}</td>
							<td><fmt:formatDate value="${colegiado.dataFim}" pattern="dd/MM/yyyy"/></td>
							<td><fmt:formatDate value="${colegiado.dataFim}" pattern="dd/MM/yyyy"/></td>
							<td>${colegiado.curso.nome}</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<jsp:include page = "../rodape.xhtml" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page = "../topo.xhtml" />

	<div class="container">
		<div class="main-page">
			<div class="row">
            	<h4 class="pull-left page-title">Cadastrar Colegiado &nbsp;&nbsp;</h3>
	                <ol class="breadcrumb pull-right">
	                    <li><a href="#">Collegialis</a></li>
	                    <li><a href="listar.jsp">Colegiados</a></li>
	                    <li class="active">Cadastrar</li>
	                </ol>
            </div>

			<!-- Mensagens de erro do formulario -->
			<c:if test="${not empty msgsErro}">
				<div style="color: red">
					<ul>
						<c:forEach var="msg" items="${msgsErro}">
							<li>${msg}</li>
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Dados do colegiado</h3>
				</div>
				<div class="panel-body">

					<form action="${pageContext.request.contextPath}/controller.do" method="POST" class="form-horizontal">
						<input type="hidden" name="op" value="novcol">
						<div class="row">
							<div class="form-group">
								<label for="descricao" class="col-md-2 control-label">Descri��o:</label>
								<div class="col-md-10">
									<input id="descricao" value="${colegiado.descricao}" name="descricao" type="text" class="form-control" placeholder="Texto simples para identificar o colegiado" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="portaria" class="col-md-2 control-label">Portaria:</label>
								<div class="col-md-10">
									<input id="portaria" value="${colegiado.portaria}" name="portaria" class="form-control" type="text" placeholder="�rg�o do IFPB e data" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="dataini" class="col-md-2 control-label">Data in�cio:</label>
								<fmt:formatDate var="dif" value="${colegiado.dataInicio}" pattern="dd/MM/yyyy"/>  
								<div class="col-md-10">
									<input id="dataini" value="${dif}" name="dataini" class="form-control" type="date" placeholder="dd/mm/aaaa" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<label for="datafim" class="col-md-2 control-label">Data fim:</label> 
								<fmt:formatDate var="dff" value="${colegiado.dataFim}" pattern="dd/MM/yyyy"/>
								<div class="col-md-10">
									<input id="datafim" value="${dff}" name="datafim" class="form-control" type="date" placeholder="dd/mm/aaaa" />
								</div>
							</div>
						</div>

						<div class="row">
							<div  class="form-group">
									<label class="col-md-2 control-label" for="curso">Curso:</label> 
									<div class="col-md-10">
										<select class="form-control" id="curso" name="curso">
										<c:forEach var="curso" items="${utilBean.cursos}">
											<c:if test="${curso.id eq colegiado.curso.id}">
												<option value="${curso.id}" label="${curso.nome}" selected>${curso.nome}</option>
											</c:if>
											<c:if test="${curso.id ne colegiado.curso.id}">
												<option value="${curso.id}" label="${curso.nome}">${curso.nome}</option>
											</c:if>
											
										</c:forEach>
										</select>
									</div>
							</div>
						</div>

						<div class="row">
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<br /> <input type="submit" class="btn btn-success" value="Cadastrar">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
<jsp:include page = "../rodape.xhtml" />
	<c:set var="endofconversation" value="true" scope="request"/>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>

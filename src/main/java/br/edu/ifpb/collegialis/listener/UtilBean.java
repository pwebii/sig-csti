package br.edu.ifpb.collegialis.listener;

import java.util.List;

import br.edu.ifpb.collegialis.dao.AlunoDAO;
import br.edu.ifpb.collegialis.dao.AssuntoDAO;
import br.edu.ifpb.collegialis.dao.ColegiadoDAO;
import br.edu.ifpb.collegialis.dao.CursoDAO;
import br.edu.ifpb.collegialis.dao.PersistenceUtil;
import br.edu.ifpb.collegialis.dao.ProcessoDAO;
import br.edu.ifpb.collegialis.dao.ProfessorDAO;
import br.edu.ifpb.collegialis.entity.Aluno;
import br.edu.ifpb.collegialis.entity.Assunto;
import br.edu.ifpb.collegialis.entity.Colegiado;
import br.edu.ifpb.collegialis.entity.Curso;
import br.edu.ifpb.collegialis.entity.Processo;
import br.edu.ifpb.collegialis.entity.Professor;

public class UtilBean {

	public List<Curso> getCursos() {
		CursoDAO dao = new CursoDAO(PersistenceUtil.getCurrentEntityManager());
		List<Curso> cursos = dao.findAll();
		return cursos;
	}
	
	public List<Colegiado> getColegiados() {
		ColegiadoDAO dao = new ColegiadoDAO(PersistenceUtil.getCurrentEntityManager());
		List<Colegiado> colegiados = dao.findAll();
		return colegiados;
	}

	public List<Processo> getProcessos() {
		ProcessoDAO dao = new ProcessoDAO(PersistenceUtil.getCurrentEntityManager());
		List<Processo> processos = dao.findAll();
		return processos;
	}

	public List<Assunto> getAssuntos() {
		AssuntoDAO dao = new AssuntoDAO(PersistenceUtil.getCurrentEntityManager());
		List<Assunto> assuntos = dao.findAll();
		return assuntos;
	}

	public List<Professor> getRelatores() {
		ProfessorDAO dao = new ProfessorDAO(PersistenceUtil.getCurrentEntityManager());
		List<Professor> relatores = dao.findAll();
		return relatores;
	}

	public List<Aluno> getAlunos() {
		AlunoDAO dao = new AlunoDAO(PersistenceUtil.getCurrentEntityManager());
		List<Aluno> alunos = dao.findAll();
		return alunos;
	}

}

package br.edu.ifpb.collegialis.facade;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.edu.ifpb.collegialis.dao.ProcessoDAO;
import br.edu.ifpb.collegialis.dao.AlunoDAO;
import br.edu.ifpb.collegialis.dao.AssuntoDAO;
import br.edu.ifpb.collegialis.dao.MembroDAO;
import br.edu.ifpb.collegialis.dao.PersistenceUtil;
import br.edu.ifpb.collegialis.entity.Aluno;
import br.edu.ifpb.collegialis.entity.Assunto;
import br.edu.ifpb.collegialis.entity.Membro;
import br.edu.ifpb.collegialis.entity.Processo;

public class FacadeProcesso {
	
	private Processo processo;
	
	private List<String> mensagensErro;
	
	public Resultado cadastrar(Map<String, String[]> parametros) {
		
		Resultado resultado = new Resultado();
		
		// Se passar na validacao, o objeto Processo pode ser persistido
		if (this.validarParametros(parametros)) {
			ProcessoDAO dao = new ProcessoDAO(PersistenceUtil.getCurrentEntityManager());
			dao.beginTransaction();
			dao.insert(this.processo);
			dao.commit();
			resultado.setErro(false);
			resultado.setMensagensErro(Collections.singletonList("Processo criado com sucesso"));
		} else {
			resultado.setEntitade(this.processo);
			resultado.setErro(true);
			resultado.setMensagensErro(this.mensagensErro);
		}
		return resultado;
	}

	public Resultado atualizar(Map<String, String[]> parametros) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private boolean validarParametros(Map<String, String[]> parametros) {
		// Parametros do form para criar um processo
		String[] numero = parametros.get("numero");
		String[] dataRecepcao = parametros.get("dataRecepcao");
		String[] requisitante = parametros.get("requisitante");
		String[] assunto = parametros.get("assunto");
		String[] relator = parametros.get("relator");
		this.processo = new Processo();
		
		this.mensagensErro = new ArrayList<String>();

		// numero � obrigatorio
		if (numero == null || numero.length == 0 || numero[0].isEmpty()) {
			this.mensagensErro.add("Numero � campo obrigat�rio!");
		} else {
			this.processo.setNumero(numero[0]);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		// datarecebimento � obrigatorio
		if (dataRecepcao == null || dataRecepcao.length == 0 || dataRecepcao[0].isEmpty()) {
			this.mensagensErro.add("Data de Recebimento � campo obrigat�rio!");
		} else {
			Date dataRecep;
			try {
				dataRecep = sdf.parse(dataRecepcao[0]);
				this.processo.setDataRecepcao(dataRecep);
			} catch (ParseException e) {
				this.mensagensErro.add("Formato inv�lido para a data in�cio");
			}
		}
		// Se algum assunto foi selecoinado, busca-o na base de dados
		if (assunto != null && assunto.length != 0 && !assunto[0].isEmpty()) {
			AssuntoDAO dao = new AssuntoDAO(PersistenceUtil.getCurrentEntityManager());
			Assunto a = dao.find(Integer.parseInt(assunto[0]));
			this.processo.setAssunto(a);
		}
		// Se algum requisitante foi selecoinado, busca-o na base de dados
		if (requisitante != null && requisitante.length != 0 && !requisitante[0].isEmpty()) {
			AlunoDAO dao = new AlunoDAO(PersistenceUtil.getCurrentEntityManager());
			Aluno a = dao.find(Integer.parseInt(requisitante[0]));
			this.processo.setRequisitante(a);
		}
		// Se algum relator foi selecoinado, busca-o na base de dados
		if (relator != null && relator.length != 0 && !relator[0].isEmpty()) {
			MembroDAO dao = new MembroDAO(PersistenceUtil.getCurrentEntityManager());
			Membro r = dao.find(Integer.parseInt(relator[0]));
			this.processo.setRelator(r);
		}
			
		return this.mensagensErro.isEmpty();

	}
	

}
